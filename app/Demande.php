<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demande extends Model
{
    protected $dates = [
        'date_demande',
        'date_rendez_vous',
        'adresse'
    ];

    public function technicien()
    {
        return $this->hasOne(User::class, 'technicien_id');
    }
    public function client()
    {
        return $this->belongsTo(User::class, 'client_id');
    }
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
