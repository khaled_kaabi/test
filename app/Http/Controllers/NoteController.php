<?php

namespace App\Http\Controllers;

use App\Demande;
use App\Note;
use App\Service;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function create(Request $request)
    {
        $body = json_decode($request->getContent());
        $service = Service::where('id', '=', $body->service_id)->first();
        $note = new Note();
        $note->note = $body->note;
        $user = User::find($body->client_id);
        $note->service()->associate($service);
        $note->client()->associate($user);
        $note->save();
        return new JsonResponse([
            'success' => true
        ]);
    }

    public function getAll() {
        $notes = Note::all();
        return new JsonResponse([
            'success' => true,
            'notes' => $notes
        ]);
    }
}
