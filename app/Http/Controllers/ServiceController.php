<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function create(Request $request)
    {
        $body = json_decode($request->getContent());
        $image = $request->image;
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.'png';
        \File::put(storage_path(). '/' . $imageName, base64_decode($image));
        $service = new Service();
        $service->label = $body->label;
        $service->image = 'storage/' . $imageName;
        $service->save();
        return new JsonResponse([
            'success' => true
        ]);
    }

    public function getAll()
    {
        $services = Service::all();
        return new JsonResponse([
            'services' => $services
        ]);
    }

    public function get(Request $request)
    {
        $body = json_decode($request->getContent());
        $service = Service::find($body->id);
        return new JsonResponse([
            'service' => $service
        ]);
    }
}
