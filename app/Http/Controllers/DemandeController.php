<?php

namespace App\Http\Controllers;

use App\Demande;
use App\Service;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Date;

class DemandeController extends Controller
{
    public function create(Request $request)
    {
        $body = json_decode($request->getContent());
        $service = Service::where('id', '=', $body->service_id)->first();
        $demande = new Demande();
        $demande->date_demande = new \DateTime('now');
        $user = User::find($body->client_id);
        $demande->adresse = $body->adresse;
        $demande->service()->associate($service);
        $demande->client()->associate($user);
        $demande->save();
        return new JsonResponse([
            'success' => true
        ]);
    }

    public function addTechnicien(Request $request)
    {
        $body = json_decode($request->getContent());
        $demande = Demande::find($body->demande_id);
        $technicien = User::find($body->technicien_id);
        $demande->technicien()->associate($technicien);
        $demande->save();
        return new JsonResponse([
            'success' => true
        ]);
    }

    public function dateRendezVous(Request $request)
    {
        $body = json_decode($request->getContent());
        $demande = Demande::find($body->demande_id);
        $demande->date_rendez_vous = \DateTime::createFromFormat('Y-m-d H:m:s', $body->date_rendez_vous)->format('Y-m-d H:m:s');
        $demande->save();
        return new JsonResponse([
            'success' => true
        ]);
    }

    public function getByUser(Request $request)
    {
        $body = json_decode($request->getContent());
        $demands = Demande::where('technicien_id', '=', $body->user_id)
            ->orWhere('client_id', '=', $body->user_id)->get();
        return new JsonResponse([
            'success' => true,
            'demands' => $demands
        ]);
    }

    public function getAll() {
        $demands = Demande::all();
        return new JsonResponse([
            'success' => true,
            'demands' => $demands
        ]);
    }
}
