<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function register(Request $request)
    {
        $body = json_decode($request->getContent());
        $user = User::where('email', '=', $body->email)->get();
        if ($user->isNotEmpty()) {
            return new JsonResponse([
                'erreur' => 'email utilisé'
            ], 200);
        } else {
            $user = User::create([
                'name' => $body->name,
                'email' => $body->email,
                'password' => bcrypt($body->password),
                'role' => $body->role,
                'telephone' => $body->telephone
            ]);
            return new JsonResponse([
                'success' => true,
                'user' => $user
            ], 200);
        }
    }

    public function login (Request $request)
    {
        $body = json_decode($request->getContent());
        if (Auth::attempt(array('email' => $body->email, 'password' => $body->password))){
            return new JsonResponse([
                'success' => true,
                'user' => Auth::user()
            ], 200);
        }else{
            return new JsonResponse([
                'erreur' => 'Mot de passe incorrect',
            ], 200);
        }
    }

    public function createTechnicien(Request $request)
    {
        $body = json_decode($request->getContent());
        $user = User::where('email', '=', $body->email)->get();
        if ($user->isNotEmpty()) {
            return new JsonResponse([
                'erreur' => 'email utilisé'
            ], 200);
        } else {
            $user = User::create([
                'name' => $body->name ?? '',
                'email' => $body->email,
                'password' => bcrypt($body->password),
                'role' => $body->role ?? 'technicien',
                'telephone' => $body->telephone ?? ''
            ]);
            return new JsonResponse([
                'success' => true,
            ], 200);
        }
    }

    public function listTechniciens()
    {
        $techniciens = User::where('role', '=', 'technicien');
        return new JsonResponse([
            'success' => true,
            'techniciens' => $techniciens
        ], 200);
    }

    public function listClients()
    {
        $clients = User::where('role', '=', 'client');
        return new JsonResponse([
            'success' => true,
            'techniciens' => $clients
        ], 200);
    }
}
