<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'LoginController@register')->name('register');
Route::post('/login', 'LoginController@login')->name('login');
Route::post('/addTechnicien', 'LoginController@createTechnicien')->name('addTechnicien');
Route::post('/listTechniciens', 'LoginController@listTechniciens')->name('listTechniciens');
Route::post('/listClients', 'LoginController@listClients')->name('listClients');

Route::post('/addService', 'ServiceController@create')->name('addService');
Route::post('/getService', 'ServiceController@get')->name('getService');
Route::post('/getAllServices', 'ServiceController@getAll')->name('getAllServices');

Route::post('/addServiceToClient', 'DemandeController@create')->name('addServiceToClient');
Route::post('/addTechnicienToDemande', 'DemandeController@addTechnicien')->name('addTechnicien');
Route::post('/addDateRendezVous', 'DemandeController@dateRendezVous')->name('dateRendezVous');
Route::post('/getByUser', 'DemandeController@getByUser')->name('getByUser');
Route::post('/getAllDemandes', 'DemandeController@getAll')->name('getAllDemandes');


Route::post('/getAllNotes', 'NoteController@getAll')->name('getAllNotes');
Route::post('/addNote', 'NoteController@create')->name('addNote');
